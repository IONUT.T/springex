package com.example.ro36school.mapper.impl;

import com.example.ro36school.dto.TeacherDTO;
import com.example.ro36school.entity.Teacher;
import com.example.ro36school.mapper.TeacherMapper;
import org.springframework.stereotype.Component;

@Component
public class TeacherMapperImpl implements TeacherMapper {
    @Override
    public TeacherDTO toDto(Teacher entity) {
        return new TeacherDTO(entity.getId(), entity.getFirstName(), entity.getLastName());
    }
}
