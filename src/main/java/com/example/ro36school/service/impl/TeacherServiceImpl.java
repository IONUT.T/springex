package com.example.ro36school.service.impl;

import com.example.ro36school.dto.TeacherDTO;
import com.example.ro36school.entity.Teacher;
import com.example.ro36school.mapper.TeacherMapper;
import com.example.ro36school.repository.TeacherRepository;
import com.example.ro36school.service.TeacherService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {

    TeacherRepository teacherRepository;
    TeacherMapper teacherMapper;
    public TeacherServiceImpl(TeacherRepository teacherRepository,TeacherMapper teacherMapper) {
        this.teacherRepository = teacherRepository;
        this.teacherMapper = teacherMapper;
    }

    @Override
    public List<TeacherDTO> findAllTeachers() {
        return teacherRepository.findAll().stream().map(t -> teacherMapper.toDto(t) ).collect(Collectors.toList());
    }
}
