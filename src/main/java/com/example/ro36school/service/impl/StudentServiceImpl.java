package com.example.ro36school.service.impl;

import com.example.ro36school.dto.StudentDTO;
import com.example.ro36school.mapper.StudentMapper;
import com.example.ro36school.repository.StudentRepository;
import com.example.ro36school.service.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;

    public StudentServiceImpl(StudentRepository studentRepository, StudentMapper studentMapper) {
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
    }

    @Override
    public List<StudentDTO> findAll() {
//        return studentRepository.findAll().stream().map(studentMapper::toDto).collect(Collectors.toList());
        return studentRepository.findAll().stream().map(s -> studentMapper.toDto(s)).collect(Collectors.toList());
    }

    @Override
    public List<StudentDTO> findAllByClassId(Integer classId) {

        return null;
    }

    @Override
    public StudentDTO findById(Integer id) {
        return studentMapper.toDto(studentRepository.findById(id).get());
    }

    @Override
    public List<StudentDTO> findAllByFirstName(String firstName) {
        return studentRepository.findByFirstName(firstName).stream().map(studentMapper::toDto).collect(Collectors.toList());
    }
}
