package com.example.ro36school.service;

import com.example.ro36school.dto.StudentDTO;
import com.example.ro36school.entity.Student;

import java.util.List;

public interface StudentService {

    List<StudentDTO> findAll();

    List<StudentDTO> findAllByClassId(Integer classId);

    StudentDTO findById(Integer id);

    List<StudentDTO> findAllByFirstName(String firstName);
}
