package com.example.ro36school.service;

import com.example.ro36school.dto.TeacherDTO;
import com.example.ro36school.entity.Teacher;

import java.util.List;

public interface TeacherService {

    public List<TeacherDTO> findAllTeachers();
}
