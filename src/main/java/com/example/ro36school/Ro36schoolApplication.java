package com.example.ro36school;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ro36schoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ro36schoolApplication.class, args);
	}

}
