package com.example.ro36school.controller;

import com.example.ro36school.dto.TeacherDTO;
import com.example.ro36school.entity.Teacher;
import com.example.ro36school.service.TeacherService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/teachers")
public class TeacherController {

    TeacherService teacherService;

    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping //ii spune springului ce fel de request se face ptr metoda asta
    public ResponseEntity<List<TeacherDTO>> findAllTeachers(){

        List<TeacherDTO> teacherList = teacherService.findAllTeachers();
        ResponseEntity responseEntity = new ResponseEntity<>( teacherList,HttpStatus.OK);

       return responseEntity;
    }




    // find all teacher

    // find all teachers by subject
}
