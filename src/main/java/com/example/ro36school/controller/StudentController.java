package com.example.ro36school.controller;

import com.example.ro36school.dto.StudentDTO;
import com.example.ro36school.service.StudentService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/students")
public class StudentController {

    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public ResponseEntity<List<StudentDTO>> findAll(@RequestParam(value = "first_name", required = false) String firstName) {
        if(firstName != null) {
            return ResponseEntity.ok(studentService.findAllByFirstName(firstName));
        } else {
            return ResponseEntity.ok(studentService.findAll());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> findById(@PathVariable(name = "id") Integer idParam) {
        return ResponseEntity.ok(studentService.findById(idParam));
    }


}
