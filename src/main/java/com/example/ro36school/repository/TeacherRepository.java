package com.example.ro36school.repository;

import com.example.ro36school.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// interactiunea cu baza de date
@Repository
//creeaza un obiect de tipul teacheRepository
public interface TeacherRepository extends JpaRepository<Teacher,Integer> {




}
