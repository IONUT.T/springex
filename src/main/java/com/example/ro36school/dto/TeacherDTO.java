package com.example.ro36school.dto;
//data transfer object

import lombok.*;
import org.springframework.stereotype.Service;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class TeacherDTO {

    private Integer id;

    private String firstName;

    private String lastName;
}
